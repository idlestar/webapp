# README #

Read below guideline for project run.

### What is this repository for? ###

* This repository for Todo App. Where you can add new TODO, Update them, Delete them and Also mark as Completed.

### How do I get set up? ###

* Install NPM first.
* Download OR clone this repository and after that go to directory.
* Execute npm install, Which is used for install project dependencies.
* Execute ng serve, Which runs you application on localhost:4200.
